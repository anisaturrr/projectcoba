<html>
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Malas Ngoding - Tutorial Laravel #18 : Membuat Form Validasi Pada Laravel</title>
 
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

	<!-- <title>Tutorial Membuat CRUD Pada Laravel - www.malasngoding.com</title> -->
</head>
<body>
 
	<h2><a href="https://www.malasngoding.com">www.malasngoding.com</a></h2>
	<!-- <h3>Edit Pegawai</h3> -->
 
	<a href="{{route('hotel')}}"> Kembali</a>
	
	<br/>
    
        {{-- menampilkan error validasi --}}
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

	<br/>
 
	<form action="{{route('hotelupdate')}}" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $hotel->hotel_id }}"> <br/>
		Nama <input type="text" required="required" name="nama" value="{{ $hotel->nama }}"> <br/>
		Kota <input type="text" required="required" name="kota" value="{{ $hotel->kota }}"> <br/>
		<input class="btn btn-primary" type="submit" value="Proses">
	</form>
		
 
</body>
</html>