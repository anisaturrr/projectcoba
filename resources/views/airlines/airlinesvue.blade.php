<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daengweb.id</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
</head>
<body>
        <div class="container" style="padding-top: 20px">
            <div class="row">
                <div id="input-data">
                        <label for="">Nama Lengkap</label>
                        <input type="text" v-model="nama" class="form-control">
                        <label for="">No Telp</label>
                        <input type="text" v-model="telp" class="form-control">
                        <br>
                        <button class="btn btn-primary btn-sm">Save</button>
                </div>
            ​
                <div id="show-data">
                    @{{ testshow }}
                </div>
            </div>
        </div>
​
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
    <script>
        var form = new Vue({
            el: '#input-data',
            data: {
                testinput: 'Cek Form'
            }
        })

        var show = new Vue({
            el: '#show-data',
            data: {
                testshow: 'Cek Show'
            }
        })
    </script>
</body>
</html>