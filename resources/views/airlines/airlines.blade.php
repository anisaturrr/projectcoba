<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <title>Flash Message</title>
	<title>Tutorial Membuat CRUD Pada Laravel - www.malasngoding.com</title>
</head>
<body>

    <style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style>

	<a  href="{{route('halo_suku')}}"> back home</a>
	<br/>

	<a  href="{{route('airlines_tambah')}}"> + Tambah Airlines Baru</a>
	<br/>
	
	<br/>
	<table border="1">
		<tr>
			<th>Id</th>
			<th>Nama</th>
		</tr>
		@foreach($airlines as $p)
		<tr>
			<td>{{ $p->airlines_id }}</td>
			<td>{{ $p->nama }}</td>
		</tr>
		@endforeach
	</table>
 
 
  <!-- Scripts -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>