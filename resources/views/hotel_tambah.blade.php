<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Malas Ngoding - Tutorial Laravel #18 : Membuat Form Validasi Pada Laravel</title>
 
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

	<!-- <title>Tutorial Membuat CRUD Pada Laravel - www.malasngoding.com</title> -->
</head>
<body>

    
<div class="container" id="app"> 
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card mt-5">
                    <div class="card-body">
                        <h3 class="text-center">Tambah Data Hotel</h3>
                        <br/>
                        
                        {{-- menampilkan error validasi --}}
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        <br/>
                            <!-- form validasi -->
                        <form action="{{route('hotelstore')}}" method="post">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="nama">Nama Hotel</label>
                                <input class="form-control" type="text" name="namahotel" v-model="namahotel">
                                <span style="color:red;" v-if="errorMessage.namahotel" v-for="err in errorMessage.namahotel">@{{err}}</span>
                                <!-- <input class="form-control" type="text" name="nama" value="{{ old('nama') }}"> -->
                            </div>
                            <div class="form-group">
                                <label for="pekerjaan">Kota</label>
                                <input class="form-control" type="text" name="kota" v-model="kota"> 
                                <span style="color:red;" v-if="errorMessage.kota" v-for="err in errorMessage.kota">@{{err}}</span>
                            </div>
                            <div class="form-group">
                            
                                <a href="{{route('hotel')}}" class="btn btn-info"> Kembali</a>
                                <!-- <input class="btn btn-primary" type="submit" value="Proses"> -->
                                <a href="#" class="btn btn-primary" @click="tambahhotel"> Submit</a>
                                <!-- <input class="btn btn-primary" type="submit" value="Simpan"> -->
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
    <script>
    new Vue({
        el: '#app',
        data(){
            return {
                namahotel:'',
                kota:'',
                errorMessage: {}
            }
        },
        methods:{
            tambahhotel(){
                var postData = new FormData();
                postData.set('namahotel',this.namahotel);
                postData.set('kota',this.kota);
                var vm = this;
                axios.post('{{route('hotelstore')}}',postData, {
                    headers: {'X-Requested-With': 'XMLHttpRequest'}
                }).then((response)=>{
                        console.log(response);
                        window.location = response.data.redirect;
                    }).catch((error)=>{
                        vm.errorMessage = error.response.data.errors;
                    });
            }
        }
    })
    </script>
</body>
</html>