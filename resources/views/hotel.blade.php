<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <title>Flash Message</title>
	<title>Tutorial Membuat CRUD Pada Laravel - www.malasngoding.com</title>
</head>
<body>

    <style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style>

	<a  href="{{route('halo_suku')}}"> back home</a>
	<br/>

	<a  href="{{route('hotel_tambah')}}"> + Tambah Pegawai Baru</a>
	<br/>
	
	<br/>
    <p>Cari Data Pegawai :</p>
	<form action="{{route('hotelcari')}}" method="GET">
		<input type="text" name="cari" placeholder="Cari Nama Hotel .." value="{{ old('cari') }}">
		<input type="submit" value="CARI">
	</form>
	<table border="1">
		<tr>
			<th>Id</th>
			<th>Nama</th>
			<th>Kota</th>
			<th>Action</th>
		</tr>
		@foreach($hotel as $p)
		<tr>
			<td>{{ $p->hotel_id }}</td>
			<td>{{ $p->nama }}</td>
			<td>{{ $p->kota }}</td>
            <td>
                <a href="{{route('hoteledit', ['id' => $p->hotel_id])}}">Edit</a> 
                <form action="{{route('hotelhapus',  ['id' => $p->hotel_id])}}" method="POST">
                @method('PUT')
                @csrf
                <input type="submit" class="btn btn-danger" value="Delete"/>
                </form>
            </td>

		</tr>
		@endforeach
	</table>
 
	{{ $hotel->links() }}
 
  <!-- Scripts -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>