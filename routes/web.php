<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (\Illuminate\Http\Request $request) {
    $alex = new \Indonesia\Suku\Batak;
    return $alex->orangbatak($request);
    // return view('welcome');
});


// Route::get('/', function () {
//     return view('halo_suku');
// });

App::bind('OrangInterface', \Indonesia\Interfaces\SukuOrang::class);
Route::get('/', function()
{
    $car = App::make('OrangInterface');  
    // $car->bahasa();
    // $car->gas();
    return view('halo_suku', ['bahasa' => $car->bahasa(), 'gas' => $car->gas()]);
})->name('halo_suku');

Route::get('/hotel','HotelContoller@index')->name('hotel');

Route::get('/hotel/tambah','HotelContoller@tambah')->name('hotel_tambah');

Route::post('/hotel/store','HotelContoller@store')->name('hotelstore');

Route::get('/hotel/edit/{id}','HotelContoller@edit')->name('hoteledit');
Route::post('/hotel/update','HotelContoller@update')->name('hotelupdate');
Route::put('/hotel/hapus/{id}','HotelContoller@hapus')->name('hotelhapus');
Route::get('/hotel','HotelContoller@index')->name('hotel');
Route::get('/hotel/cari','HotelContoller@cari')->name('hotelcari');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

