<?php


Route::get('/airlines', 'AirlinesController@index')->name('airline');
Route::get('/airlines/tambah', 'AirlinesController@tambah')->name('airlines_tambah');
Route::post('/airlines/insert', 'AirlinesController@insert')->name('airlines_insert');
Route::get('/airlines/detil/{id}', 'AirlinesController@detil')->name('airlines_detil');
Route::get('/airlines/edit/{id}', 'AirlinesController@edit')->name('airlines_edit');
Route::post('/airlines/update', 'AirlinesController@update')->name('airlines_update');


Route::get('/airlinesvue','CobavueController@index')->name('airlinesvue');