<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HotelContoller extends Controller
{
    //
    public function index()
    {
    	// mengambil data dari table pegawai
    	$hotel = DB::table('hotel')->paginate(5);
 
    	// mengirim data pegawai ke view index
    	return view('hotel',['hotel' => $hotel]);
 
    }

    public function cari(Request $request)
	{
		// menangkap data pencarian
		$cari = $request->cari;
 
    		// mengambil data dari table pegawai sesuai pencarian data
		$hotel = DB::table('hotel')
		->where('nama','like',"%".$cari."%")
		->paginate(5);
 
    		// mengirim data pegawai ke view index
		return view('hotel',['hotel' => $hotel]);
 
	}

    public function tambah()
    {
    	// mengirim data pegawai ke view index
    	return view('hotel_tambah');
 
    }
    
    public function store(Request $request)
    {
        $this->validate($request,[
            'namahotel' => 'required|min:5|max:50',
            'kota' => 'required'
         ]);

    	// insert data ke table pegawai
        $id = DB::table('hotel')->insertGetId([
            'nama' => $request->namahotel,
            'kota' => $request->kota
        ]);
        // alihkan halaman ke halaman pegawai
        if($request->ajax())
        {
            return response()->json([
                'redirect' => url('/hotel')
            ]);
        }
        return  redirect('/hotel');

    }

    public function edit($id)
    {
        
        // mengambil data pegawai berdasarkan id yang dipilih
        $hotel = DB::table('hotel')->where('hotel_id',$id)->first();
        // passing data pegawai yang didapat ke view edit.blade.php
        $data = array(
            'hotel_id' => $id,
            'hotel'   => $hotel
        );
        return view('hotel_edit', $data );
    
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required|min:5|max:50',
            'kota' => 'required'
         ]);
         
        // update data pegawai
        DB::table('hotel')->where('hotel_id',$request->id)->update([
            'hotel_id' => $request->id,
            'nama' => $request->nama,
            'kota' => $request->kota
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/hotel');
    }

    public function hapus($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('hotel')->where('hotel_id',$id)->delete();
            
        // alihkan halaman ke halaman pegawai
        return redirect('/hotel');
    }

    
}
