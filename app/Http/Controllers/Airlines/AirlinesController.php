<?php

namespace App\Http\Controllers\Airlines;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AirlinesController extends Controller
{
    //
    public function index()
    {
        // mengambil data dari table pegawai
        dump(config('database.default'));
    	$airlines = DB::table('airlines')->get();
 
    	// mengirim data pegawai ke view index
    	return view('airlines.airlines',['airlines' => $airlines]);
 
    }

    
    public function tambah()
    {
    	// mengirim data pegawai ke view index
    	return view('airlines.airlines_tambah');
 
    }
    
    public function insert(Request $request)
    {
    	// mengirim data pegawai ke view index
    	// $this->validate($request,[
        //     'namaairlanes' => 'required|min:5|max:50'
        //  ]);

    	// insert data ke table pegawai
        $id = DB::table('airlines')->insertGetId([
            'nama' => $request->namaairlanes
        ], 'airlines_id');
        
        return  redirect('/airlines/detil/'. $id);
 
    }

    public function detil($id)
    {
        // mengambil data dari table pegawai
        
        $airlines_detil = DB::table('airlines')->where('airlines_id',$id)->first();

        // dump($airlines_detil);

        $data = array(
            'airlines_id' => $id,
            'airlines_detil' => $airlines_detil
        );
    	// mengirim data pegawai ke view index
        
        return view('airlines.airlines_detil', $data );
      
    }

    public function edit($id)
    {
        // mengambil data dari table pegawai
        
        $airlines_detil = DB::table('airlines')->where('airlines_id',$id)->first();

        // dump($airlines_detil);

        $data = array(
            'airlines_id' => $id,
            'airlines_detil' => $airlines_detil
        );
    	// mengirim data pegawai ke view index
        
        return view('airlines.airlines_edit', $data);
      
    }

    public function update(Request $request)
    {
        // update data pegawai
        DB::table('airlines')->where('airlines_id',$request->id)->update([
            'airlines_id' => $request->id,
            'nama' => $request->namaairlanes
        ]);

        // alihkan halaman ke halaman pegawai
        return  redirect('/airlines/detil/'. $request->id);
      
    }
}
